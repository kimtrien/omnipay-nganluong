<?php

namespace Omnipay\NganLuong\Message;

/**
 * NganLuong Pro Purchase Request
 */
class ProPurchaseRequest extends ProAuthorizeRequest
{

    protected $action = 'Sale';
}
